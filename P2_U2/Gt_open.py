#!/usr/bin/python3
# -*- coding: utf-8 -*-

try:
    import pandas as pd
    import gi
    from biopandas.pdb import PandasPdb
    gi.require_version('Gtk', '3.0')
    from gi.repository import Gtk

except ImportError:
    # Que hacer si el módulo no se puede importar
    print("""
    Módulo no instalado. Favor, instale las librerías necesarias para
    la ejecución del código, las cuales son:
    > Pandas
    > Biopandas
    > Gi
    > pyMOL
    """)


class MainWindows():
    def __init__(self):
        # Ventana
        self.builder = Gtk.Builder()
        self.builder.add_from_file("./AppProtein.glade")

        self.window = self.builder.get_object("main_windows")
        self.window.set_title("Visualizador de proteínas")
        self.window.connect("destroy", Gtk.main_quit)
        self.window.set_default_size(800, 600)
        self.window.show_all()

        # Instanciar botones
        self.btn_abrir_filechooser = self.builder.get_object(
            "abrir_filechooser")
        self.btn_abrir_filechooser.connect(
            "clicked", self.apertura_filechooser)

        self.btn_abrir_about = self.builder.get_object("abrir_about")
        self.btn_abrir_about.connect("clicked", self.apertura_about)

        # Combobox y Liststore
        self.combobox = self.builder.get_object("combobox_main")
        self.caracteristicas = Gtk.ListStore(str)

        cell = Gtk.CellRendererText()

        self.ruta = ""
        self.combobox.pack_start(cell, True)
        self.combobox.add_attribute(cell, "text", 0)
        caracteristicas = ["ATOM",
                           "HETATM",
                           "ANISOU",
                           "OTHERS"]

        for caracteristica in caracteristicas:
            self.caracteristicas.append([caracteristica])

        self.combobox.set_model(model=self.caracteristicas)
        self.combobox.connect("changed", self.seleccion)

        self.tree_view = self.builder.get_object("tree_view")

        # self.imagen_proteina = self.builder.get_object("imagen_proteina")
        # self.resumen_proteina = self.get_object ("resumen_proteina")
        # self.dataframe

    # abrir .pdb y trasnformarlo a dataframe
    def update_tree(self, pdb_file):
        # importar Filechooser() --> acept_filechooser() --> self.ruta
        # data_protein = ppdb.read_pdb(ruta)

        # eliminando columnas
        if self.tree_view.get_columns():
            print(self.tree_view.get_columns())
            for column in self.tree_view.get_columns():
                self.tree_view.remove_column(column)

        # Se crean columnas del treeView y se carga su respectiva lista modelo
        len_columns = len(data_protein.columns)
        model = Gtk.ListStore(*(len_columns * [str]))
        # self.list = Gtk.ListStore(str, str, str, str, str, str, str)
        self.tree_view.set_model(model=model)

        cell = Gtk.CellRendererText()
        # Se añaden las columnas al treeView con sus respectivos títulos
        for item in range(len_columns):
            column = Gtk.TreeViewColumn(data_protein.columns[item],
                                        cell,
                                        text=item)
            self.tree_view.append_column(column)

        for value in data_protein.values:
            row = [str(x) for x in value]
            model.append(row)

    # seleccion dentro del combobox

    def seleccion(self, btn=None):
        # indice = self.combobox.get_active_iter()
        # modelo = self.combobox.get_model()
        # opcion = modelo[indice][0]
        chooser = FileChooser()
        self.ruta = chooser.get_ruta()
        self.chooser.destroy()
        variable = self.combobox.get_active()
        print(variable)
        print("\nHOLAAA")
        print(self.ruta)

        if self.ruta == "":
            print("El dataframe está vacío")

        else:
            if variable == 0:
                print("Ingreso a ATOM")
                self.prote = self.prote["ATOM"]
                signal = self.combobox.get_active()
                # self.imagen_proteina.set_from_file("./imagen_proteina.png")

            elif variable == 1:
                print("Ingreso a HETATM")
                self.prote = self.prote["HETATM"]
                signal = self.combobox.get_active()
                # self.imagen_proteina.set_from_file("./imagen_proteina.png")

            elif variable == 2:
                print("Ingreso a ANISOU")
                self.prote = self.prote["ANISOU"]
                signal = self.combobox.get_active()
                # self.imagen_proteina.set_from_file("./imagen_proteina.png")

            elif variable == 3:
                print("Ingreso a OTHERS")
                self.prote = self.prote["OTHERS"]
                signal = self.combobox.get_active()
                # self.imagen_proteina.set_from_file("./imagen_proteina.png")

            if len(self.tree_view.get_columns()) != 0:
                for col in self.tree_view.get_columns():
                    self.tree_view.remove_column(col)

            # Conversión de tipo de las tablas
            n_columnas = len(self.prote.columns)
            self.lista_valores = Gtk.ListStore(*(n_columnas * [str]))
            cell = Gtk.CellRendererText()

            for columna in range(n_columnas):
                col = Gtk.TreeViewColumn(self.prote.columns[columna], cell, text=columna)
                self.tree_view.append_column(col)

            self.tree_view.set_model(model=self.lista_valores)

            for value in self.prote.values:
                fila = [str(linea) for linea in value]
                self.lista_valores.append(fila)

        # Label informativo

    # Boton "Abrir" para que abra filechooser
    def apertura_filechooser(self, btn=None):
        filechooser = FileChooser()
        response = filechooser.chooser_win.run()
        print(response)

    # Boton "Acerca de" para que abra la ventana about_dialog
    def apertura_about(self, btn=None):
        about = About_Dialog()


# Clase para FILECHOOSER
class FileChooser():

    # Se define función de constructor
    def __init__(self):
        self.builder = Gtk.Builder()
        self.builder.add_from_file("./AppProtein.glade")
        self.chooser_win = self.builder.get_object("file_chooser")
        self.chooser_win.set_default_size(700, 500)
        self.chooser_win.show_all()

        # Filtrado de archivos~carpetas, que solo permitirá la
        # selección de archivos en formato .pdb
        pdb_filter = Gtk.FileFilter()
        pdb_filter.set_name('Formato válido (.pdb)')
        pdb_filter.add_pattern('*.pdb')
        self.chooser_win.add_filter(pdb_filter)

        self.accept_btn = self.builder.get_object("accept_btn")
        self.cancel_btn = self.builder.get_object("cancel_btn")
        #! self.cancel_btn.connect('clicked', self.cancel_pressed)
        self.accept_btn.connect("clicked", self.acept_filechooser)
        self.cancel_btn.connect("clicked", self.cancel_filechooser)

    def acept_filechooser(self, btn=None):
        # self.ruta = ""
        self.ruta = self.chooser_win.get_filename()
        self.chooser_win.destroy()
        print(self.ruta)
        ppdb = PandasPdb()
        ppdb.read_pdb(self.ruta)
        self.prote = ppdb.df
        # print(self.prote)
        return self.ruta
        # Hay que improtar eso a la funcion dentro de (linea 72) MainWindows()
        # para que se abra como Dataframe y salga en el treeview

    def cancel_filechooser(self, btn=None):
        self.chooser_win.destroy()

    def get_ruta(self):
        self.ruta = self.chooser.get_filename()

        return self.ruta



class About_Dialog():
    # Constructor
    def __init__(self):
        self.builder = Gtk.Builder()
        self.builder.add_from_file("./AppProtein.glade")

        self.emergente = self.builder.get_object("about_dialog")
        self.emergente.show_all()

        self.btn_aceptar = self.builder.get_object("aceptar")
        self.btn_aceptar.connect("clicked", self.ok_press)

    # Cerrar
    def ok_press(self, btn=None):
        self.emergente.destroy()


if __name__ == "__main__":
    MainWindows()
    Gtk.main()
