#!/usr/bin/python3
# -*- coding: utf-8 -*-

from biopandas.pdb import PandasPdb

#archivo_proteina = (pdb)


def filtrado_atom():
    ppdb = PandasPdb()
    ppdb.read_pdb("./Files_PDB/5ylu.pdb")
    print(ppdb.df.keys())
    print("\nRaw PDB file contents:\n\n%s\n..." % ppdb.pdb_text[:1000])
    print(ppdb.df["ATOM"])

    ppdb.to_pdb(path="./Files_PDB/5ylu.pdb",
                records=["ATOM"],
                gz = False,
                append_newline = True)
    
def filtrado_hetatm():
    ppdb = PandasPdb()
    ppdb.read_pdb("./Files_PDB/5ylu.pdb")
    print(ppdb.df.keys())
    print("\nRaw PDB file contents:\n\n%s\n..." % ppdb.pdb_text[:1000])
    print(ppdb.df["HETATM"])

    ppdb.to_pdb(path="./Files_PDB/5ylu.pdb",
                records=["HETATM"],
                gz = False,
                append_newline = True)

def filtrado_anisou():
    ppdb = PandasPdb()
    ppdb.read_pdb("./Files_PDB/5ylu.pdb")
    print(ppdb.df.keys())
    print("\nRaw PDB file contents:\n\n%s\n..." % ppdb.pdb_text[:1000])
    print(ppdb.df["ANISOU"])

    ppdb.to_pdb(path="./Files_PDB/5ylu.pdb",
                records=["ANISOU"],
                gz = False,
                append_newline = True)

def filtrado_others():
    ppdb = PandasPdb()
    ppdb.read_pdb("./Files_PDB/5ylu.pdb")
    print(ppdb.df.keys())
    print("\nRaw PDB file contents:\n\n%s\n..." % ppdb.pdb_text[:1000])
    print(ppdb.df["OTHERS"])

    ppdb.to_pdb(path="./Files_PDB/5ylu.pdb",
                records=["OTHERS"],
                gz = False,
                append_newline = True)