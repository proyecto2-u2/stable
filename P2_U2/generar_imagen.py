#!/usr/bin/python3
# -*- coding:utf-8 -*-

import __main__
# Pymol : quiet and no GUI
__main__.pymol_argv = ["pymol", "-qc"] # noqa : E402
import pymol
pymol.finish_launching()


def gen_image(name, filename, path_save):
    # Generar imagenes de archivo seleccionado .
    aux = name.split(".")
    pdb_name = aux[0]
    png_file = "".join([path_save])
    pdb_file = filename
    pymol.cmd.load(pdb_file, pdb_name)
    pymol.cmd.disable("all")
    pymol.cmd.enable(pdb_name)
    pymol.cmd.hide("all")
    pymol.cmd.show("cartoon")
    pymol.cmd.set("ray_opaque_background", 0)
    pymol.cmd.pretty(pdb_name)
    pymol.cmd.png(png_file)
    pymol.cmd.ray()