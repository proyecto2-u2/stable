from biopandas.pdb import PandasPdb

ppdb = PandasPdb()
ppdb.read_pdb("./Files_PDB/5ylu.pdb")
print(ppdb.df.keys())
print("\nRaw PDB file contents: \n\n %s \n..."%ppdb.pdb_text [:1000])
print(ppdb.df["ATOM"])
ppdb.to_pdb(path = "./Files_PDB/5ylu.pdb",
            records=["ATOM"],
            gz = False,
            append_newline = True)