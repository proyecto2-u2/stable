# Visualizador de proteínas
El siguiente proyecto consiste en un programa que procesa diversos parámetros respecto a una proteínas de interes. Entre estos parametros encontramos, la informacion con respecto a los ATOM(átomos de la proteína), HETATM ("hetero atom"), ANISOU(Anisotropic refinement) amen de presentar una representación pictórica de dicha proteína e información adicional de esta.

## Historia:
Fue escrito y desarrollado en conjunto por Jorge Carrillo Silva, Benjamín Vera Garrido y Michelle Valdés Méndez, utilizando para ello el lenguaje de programación Python.
El código del programa se rige bajo las indicaciones de la PEP-8 (Style Guide for Python Code), que busca generar un código limpio y óptimo, además de una correcta indentación y sintaxis.

## Para empezar:
Es requisito tener instalado Python 3.6+ (de preferencia la versión 3.8.5) y está pensada que su ejecución sea si o sí en un entorno Linux, tales como Debian y sus derivados, etc.
También es necesario tener instaladas las librería Matplot.lib, Pandas.lib, gi y PyGObject (Paquete de Python que proporciona enlaces para bibliotecas basadas en GObject, tales como GTK, GStreamer, WebKitGTK, Glib, GIO y más)

## Instalación:
Para instalar y ejecutar el programa en su máquina, es necesario que siga las presentes indicaciones:
1- Clonar el repositorio "stable" correspondiente a la versión final a presentarse del código, en el directorio de su preferencia, el enlace HTTPS es https://gitlab.com/proyecto2-u2/stable.
2- Luego, deberá entrar en la carpeta clonada que lleva por nombre "P2-U2" que contiene los archivos: "Gt_open.py"(archivo clave para la ejecución del programa, porque contiene todas la acciones del del programa),"AppProtein.glade"(encargado de la interfaz gráfica). Es esencial que pueda comprobar y cerciorarse que se encuentran los paquetes y librerías esenciales, ya que de lo contrario el programa podría no funcionar adecuadamente. Certifique, además, la tenencia de archivos .pdb para poder procesarlo, de otro modo no funcionaria adecuadamente el programa.
3- Posteriormente, ya está usted preparado para ejecutar el programa. Para ello, abrirá una terminal y en el directorio que tenga los archivos, ejecutará el siguiente comando.
python3 Gt_open.py, luego deberá ingresar la proteína a elección, obteniendo asi, diversa informacion de dicha proteína.

## Codificación:
El programa se construyó pensando en el soporte de la codificación UTF-8.

## Construido con:
Atom: Uno de los 3 IDE's utilizados para desarrollar el proyecto.
Visual Studio Code: Gracias a su facilidad de uso y terminal integrada, fue utilizada para construir el proyecto
VSCodium: Junto con Atom y Visual, es uno de los Entornos de desarrollo utilizados para el desarrollo del proyecto.

## Licencia:
Este proyecto está sujeto bajo la Licencia GNU GPL v3.

## Autores y creadores del proyecto:
Jorge Carrillo Silva, Benjamín Vera Garrido y Michelle Valdés Méndez.

## Agradecimientos:
Queremos agradecer a todos aquellos que nos han ayudado y nos han orientado en el desarrollo del proyecto, en especial al profesor Fabio Durán y al ayudante Arturo Lobos.
